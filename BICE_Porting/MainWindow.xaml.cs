﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Windows;
using System.Windows.Controls;
using System.Windows.Data;
using System.Windows.Documents;
using System.Windows.Input;
using System.Windows.Media;
using System.Windows.Media.Imaging;
using System.Windows.Navigation;
using System.Windows.Shapes;
using System.IO;

namespace BICE_Porting
{
    /// <summary>
    /// Interaktionslogik für MainWindow.xaml
    /// </summary>
    public partial class MainWindow : Window
    {
        List<General> generals = new List<General>(); 
        string stuffFromTextbox;
        string portrait_land_name;
        string id_start;
        public MainWindow()
        {
            InitializeComponent();
        }

        private void loading()
        {
            stuffFromTextbox = textbox.Text;
            load();
            print();
            //print_traits();

        }

        private void converting()
        {
            textbox.Text = convert_generals();
        }

        

        private void load()
        {
            int pos = 1;
            while (pos != 0)
            {
                string name;
                string skill;
                string maxskill;
                pos = stuffFromTextbox.IndexOf("name",pos);
                if (pos>0)
                {
                    //Name
                    int first = stuffFromTextbox.IndexOf("\"",pos);
                    int second = stuffFromTextbox.IndexOf("\"", first + 1);
                    name = stuffFromTextbox.Substring(first +1, second - first - 1);
                    General general = new General();
                    general.name = name;
                    
                    //Skill
                    int pos_skill = stuffFromTextbox.IndexOf("skill", pos);
                    pos_skill = stuffFromTextbox.IndexOfAny("0123456789".ToCharArray(), pos_skill);
                    skill = stuffFromTextbox.Substring(pos_skill, 1);
                    general.skill = skill;

                    //max Skill
                    int pos_maxskill = stuffFromTextbox.IndexOf("max_skill", pos_skill);
                    pos_maxskill = stuffFromTextbox.IndexOfAny("0123456789".ToCharArray(), pos_maxskill);
                    maxskill = stuffFromTextbox.Substring(pos_maxskill, 1);
                    general.maxskill = maxskill;

                    //Type
                    int pos_type = stuffFromTextbox.IndexOf("type", pos);
                    first = stuffFromTextbox.IndexOf("=", pos_type);
                    second = stuffFromTextbox.IndexOf("\n", first);
                    string sub_type = stuffFromTextbox.Substring(first + 1, second - first - 1).Trim(); 
                    string[] types = sub_type.Split(' ');
                    general.type = types[0].TrimEnd('\r');

                    //Traits
                    string sub_traits = stuffFromTextbox.Substring(pos_skill, stuffFromTextbox.IndexOf("}", pos_skill)-pos_skill);
                    int pos_traits = 0;
                    int count = 0;
                    List<string> traits = new List<string>();
                    while (sub_traits.IndexOf("add_trait", pos_traits + 1) != -1)
                    {
                        pos_traits = sub_traits.IndexOf("add_trait", pos_traits + 1);
                        first = sub_traits.IndexOf("=", pos_traits);
                        second = sub_traits.IndexOf("\n", first);
                        string sub_trait = sub_traits.Substring(first + 1, second - first - 1).Trim();
                        string[] trait = sub_trait.Split(' ');
                        //for (int i = 0; i < traits.Length; i++)
                        //{
                        //    if (traits[i]=="add_trait")
                        //    {
                        //        traits = traits.Where((source, index) => index != i).ToArray();
                        //    }
                        //    if (traits[i] == "=")
                        //    {
                        //        traits = traits.Where((source, index) => index != i).ToArray();
                        //    }
                        //}
                        for (int i = 0; i < trait.Length; i++)
                        {
                            int a = trait[i].IndexOf("\r");
                            if (a != -1)
                            {
                                trait[i] = trait[i].Substring(0, a);
                            }
                        }
                        if (trait[0] != null)
                        {
                            traits.Add(trait[0]);
                            count += 1;
                        }
                        
                        
                    }
                    general.traits = traits;

                    generals.Add(general);
                }

                pos += 1;
            }
        }

        private void print()
        {
            string msg = "";
            textbox.Clear();
            textblock_generalCount.Text = "Generals loaded " + generals.Count().ToString();
            for (int i = 0; i < generals.Count; i++)
            {
                msg += generals[i].name;
                msg += " "+ "skill=" + generals[i].skill;
                msg += " " + "maxSkill=" + generals[i].maxskill;
                msg += " " + generals[i].type;
                if (generals[i].traits != null)
                {
                    for (int j = 0; j < generals[i].traits.Count; j++)
                    {

                        msg += " " + generals[i].traits[j];
                    }
                }


                msg += Environment.NewLine;
            }
            textbox.Text = msg;
        }

        private void btn_load_Click(object sender, RoutedEventArgs e)
        {
            loading();
        }

        private void print_traits()
        {
            textbox.Clear();
            List<string> traits = new List<string>();
            traits.Add("Traits=");
            for (int i = 0; i < generals.Count; i++)
            {
                if (generals[i].traits != null)
                {
                    for (int k = 0; k < generals[i].traits.Count; k++)
                    {
                        for (int j = 0; j < traits.Count; j++)
                        {
                            if (traits[j] == generals[i].traits[k])
                            {
                                break;
                            }
                            if (j == traits.Count - 1)
                            {
                                traits.Add(generals[i].traits[k]);
                            }
                        }
                    }
                }
                
                
            }

            for (int i = 0; i < traits.Count; i++)
            {
                textbox.Text += traits[i];
                textbox.Text += Environment.NewLine;
            }
            
        }

        private string convert_generals()
        {
            string converted_generals_sting = "";
            string land = "create_corps_commander = {\r\n";
            string navy = "create_navy_leader = {\r\n";
            string name = "	name = \"";
            string picture = " picture = \"Portrait_";
            string traits = " traits = {";
            string skill = " skill = ";
            string id = " id = ";
            for (int i = 0; i < generals.Count; i++)
            {
                if (generals[i].type == "land")
                {
                    converted_generals_sting += land;
                    converted_generals_sting += name + generals[i].name + "\"\r\n";
                    converted_generals_sting += picture + portrait_land_name + "_" + remove_illegal_char(generals[i].name) + ".dds\"\r\n";
                    converted_generals_sting += traits;
                    for (int j = 0; j < generals[i].traits.Count; j++)
                    {
                        converted_generals_sting += " " + generals[i].traits[j];
                    }
                    converted_generals_sting += " " + maxskill_convert(generals[i]);
                    converted_generals_sting += " }\r\n";
                    converted_generals_sting += skill + generals[i].skill + "\r\n";
                    converted_generals_sting += id + id_start + "\r\n";
                    converted_generals_sting += "}";
                    converted_generals_sting += "\r\n";

                    count_id_forward();


                }
                else if (generals[i].type == "sea")
                {
                    converted_generals_sting += navy;
                    converted_generals_sting += name + generals[i].name + "\"\r\n";
                    converted_generals_sting += picture + portrait_land_name + "_" + remove_illegal_char(generals[i].name) + ".dds\"\r\n";
                    converted_generals_sting += traits;
                    for (int j = 0; j < generals[i].traits.Count; j++)
                    {
                        converted_generals_sting += " " + generals[i].traits[j];
                    }
                    converted_generals_sting += " " + maxskill_convert(generals[i]);
                    converted_generals_sting += " }\r\n";
                    converted_generals_sting += skill + generals[i].skill + "\r\n";
                    converted_generals_sting += id + id_start + "\r\n";
                    converted_generals_sting += "}\r\n";
                    converted_generals_sting += "\r\n";

                    count_id_forward();

                }

            }
            return converted_generals_sting;
        }

        private void count_id_forward()
        {
            
                int id = Convert.ToInt32(id_start);
                id += 1;
                id_start = Convert.ToString(id);
            
                
            
        }

        private string remove_illegal_char(string name)
        {
            name = name.Replace(" ", "_");
            name = name.Replace(".", "");
            return string.Join("", name.Split(System.IO.Path.GetInvalidFileNameChars()));
        }

        private void btn_convert_Click(object sender, RoutedEventArgs e)
        {
            portrait_land_name = textBox_LandName.Text;
            id_start = textBox_isSart.Text;
             try
             {
                int id = Convert.ToInt32(id_start);
             }
             catch
             {
                id_start = "1";
             }
            
            
                converting();
        }

        private string maxskill_convert(General gen)
        {
            string skill_level = "";

            switch (gen.maxskill)
            {
                case "1":
                    skill_level = "Mindless_General";
                    break;
                case "2":
                    skill_level = "Incompetent_General";
                    break;
                case "3":
                    skill_level = "Terrible_General";
                    break;
                case "4":
                    skill_level = "Bad_General";
                    break;
                case "5":
                    skill_level = "Decent_General";
                    break;
                case "6":
                    skill_level = "Competent_General";
                    break;
                case "7":
                    skill_level = "Awesome_General";
                    break;
                case "8":
                    skill_level = "Masterful_General";
                    break;
                case "9":
                    skill_level = "Genius_General";
                    break;
                default:
                    break;
            }

            return skill_level;
        }

    }
}
