﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace BICE_Porting
{
    public class General
    {
        public string name { get; set; }
        public string skill { get; set; }
        public string type { get; set; }
        public List<string> traits { get; set; }
        public string maxskill { get; set; }



        public General()
        {
            
        }

        public General(string _name, string _skill)
        {
            name = _name;
            skill = _skill;

        }
    }
}
